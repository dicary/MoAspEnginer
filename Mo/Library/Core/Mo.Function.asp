<%
function Md5(byval Src)
	Md5 = myMD5.md5(Src)
end function
function defined(var)
	defined = (vartype(var)<>0)
end function
function function_exists(name)
	on error resume next
	function_exists = false
	if typename(GetRef(name))="Object" then function_exists = true
	if err then
		function_exists = false
		err.clear
	end if
end function
function is_empty(var)
	is_empty = false
	if not isobject(var) then
		if not defined(var) then
			is_empty=true
		elseif isempty(var) then
			is_empty=true
		elseif isnull(var) then
			is_empty=true
		elseif var="" then
			is_empty=true
		end if
	else
		if var is nothing then is_empty=true
	end if
end function

public sub debug(str)
	Response.Write str
	Response.End()
end sub

function Rnd_(strSeed,intLength)
    Dim seedLength, pos, Strs, i
    seedLength = Len(strSeed)
    Strs = ""
    For i = 1 To intLength
		randomize timer*1000
        Strs = Strs & Mid(strSeed, Int(seedLength * Rnd) + 1, 1)
    Next
    Rnd_ = Strs
end function

function RndUCase(intLength)
    RndUCase = Rnd_("IJKLMNOPQRSTUVWXYZABCDEFGH",intLength)
end function

function RndLCase(intLength)
    RndLCase = Rnd_("abcdefghijklmnopqrstuvwxyz",intLength)
end function

function RndStr(intLength)
    RndStr = Rnd_("abcdefghiIJKLMNOPQRSTUVWXYZjklmnopqrstuvwxyzABCDEFGH",intLength)
end function

function RndNumber(intLength)
    RndNumber = Rnd_("12345678906789012678901234534567890126789012345345",intLength)
end function

function RndStr1(intLength)
    RndStr1 = Rnd_("abcdefghiIJKLMNOPQRSTUVWXYZjklmnopqrstuvwxyzABCDEFGH12345678906789012678901234534567890126789012345345",intLength)
end function

function RegTest(Str,RegStr)
	if isnull(Str) then Str=""
	Dim Reg, Mag
	Set Reg = New RegExp
	With Reg
		.IgnoreCase = True
		.Global = True
		.Pattern = RegStr
		RegTest = .test((str))
	End With
	Set Reg = nothing
end function

function ReplaceEx(sourcestr, regString, str)
    dim re
	Set re = new RegExp
	re.IgnoreCase = true
	re.Global = True
	re.pattern = "" & regString & ""
	str = re.replace(sourcestr, str)
    set re = Nothing
    ReplaceEx = str
end function

function GetMatch(ByVal Str, ByVal Rex)
	Dim Reg
	Set Reg = New RegExp
	With Reg
		.IgnoreCase = True
		.Global = True
		.Pattern = Rex
		Set GetMatch = .Execute((str))
	End With
	Set Reg = nothing
end function

public function LoadAsp(filename,charset)
	dim ret
	ret = LoadFile(filename,charset)
	ret = replaceex(ret,"(^(\s+)|(\s+)$)","")
	ret = trim(ret)
	if left(ret,2)="<%" then ret = mid(ret,3)
	if right(ret,2)=replace("% >"," ","") then ret = left(ret,len(ret)-2)
	LoadAsp = ret
end function

public function LoadFile(path,charset)
	if not F.fso.FileExists(path) then
		LoadFile = ""
		exit function
	end if
	dim stream
	set stream = F.stream()
	stream.mode = 3
	stream.type=2
	stream.charset=charset
	stream.open
	stream.loadfromfile path
	LoadFile = stream.readtext()
	stream.close
	set stream = nothing
end function

public function SaveFile(path,content,charset)
	dim stream
	set stream = F.stream()
	stream.mode = 3
	stream.type=2
	stream.charset=charset
	stream.open
	stream.writetext(content)
	stream.savetofile path,2
	stream.close
	set stream = nothing
end function

public function CreatePageList(Byval URL,ByVal RecordCount, ByVal PageSize, ByVal CurrentPage)
	Dim PageCount ,PageStr, I
	if URL="" then
		URL="?" & replaceEx(request.QueryString & "&page={#page}","(\&)?page\=(\d+)","")
		URL = replace(URL,"?&","?")
	end if
	CurrentPage = int(CurrentPage)
	RecordCount = int(RecordCount)
	PageSize = int(PageSize)
	If RecordCount Mod PageSize =0 Then
		Pagecount = RecordCount / PageSize
	Else
		Pagecount = Int(RecordCount / PageSize) + 1
	End If
	if Pagecount<=1 then exit function
	PageStr = "共[" & RecordCount & "]条记录 [" & PageSize & "]条/页 当前[" & CurrentPage & "/" & PageCount & "]页&nbsp; "
	If CurrentPage = 1 Or PageCount = 0 Then
		PageStr = PageStr & "首页&nbsp;"
		PageStr = PageStr & "上页&nbsp;"
	Else
		PageStr = PageStr & "<a href=""" & Replace(URL, "{#page}", 1) & """>首页</a>&nbsp;"
		PageStr = PageStr & "<a href=""" & Replace(URL, "{#page}", CurrentPage-1) & """>上页</a>&nbsp;"
	End If
	If CurrentPage = Pagecount  Or PageCount = 0 Then
		PageStr = PageStr & "下页&nbsp;"
		PageStr = PageStr & "尾页&nbsp;"
	Else
		PageStr = PageStr & "<a href=""" & Replace(URL, "{#page}", CurrentPage + 1) & """>下页</a>&nbsp;"
		PageStr = PageStr & "<a href=""" & Replace(URL, "{#page}", Pagecount) & """>尾页</a>&nbsp;"
	End If
	CreatePageList = PageStr
end function

function ToDate(sDateTime)
	if isdate(sDateTime) then ToDate = cdate(sDateTime) else ToDate=null
end function

function FormatDate(sDateTime, sReallyDo)
	if isdate(sDateTime) then sDateTime = cdate(sDateTime)
    FormatDate = F.FormatDate(sDateTime, sReallyDo)
end function

function UnShift(Byref Ary,item)
	redim preserve Ary(ubound(Ary)+1) 
	dim i
	for i=ubound(Ary) to 1 step -1
		if typename(Ary(i-1))="JScriptTypeInfo" then
			Set Ary(i) = Ary(i-1)
		else
			Ary(i) = Ary(i-1)
		end if
	next
	if typename(item)="JScriptTypeInfo" then
		set Ary(0) = item
	else
		Ary(0) = item
	end if
end function
function Push(Byref Ary,item)
	redim preserve Ary(ubound(Ary)+1) 
	if typename(item)="JScriptTypeInfo" then
		set Ary(ubound(Ary)) = item
	else
		Ary(ubound(Ary)) = item
	end if
end function

function Pop(Byref Ary)
	redim preserve Ary(ubound(Ary)-1) 
end function

function UnPop(Byref Ary)
	dim i
	for i=0 to ubound(Ary)-1
		if typename(Ary(i+1))="JScriptTypeInfo" then
			Set Ary(i) = Ary(i+1)
		else
			Ary(i) = Ary(i+1)
		end if
	next
	redim preserve Ary(ubound(Ary)-1) 
end function
function Join(Byref Ary,byval spli)
	dim ret
	for i=0 to ubound(Ary)
		ret = ret & Ary(i) & spli
	next
	Join = left(ret,len(ret)-len(spli))
end function
function RightCopy(Ary,Ary2)
	if ubound(Ary2)>=ubound(Ary) then
		RightCopy = Ary2
	else
		dim i,j
		j = ubound(Ary)-ubound(Ary2)
		for i=0 to ubound(Ary2)
			Ary(i+j) = Ary2(i)
		next
		RightCopy = Ary
	end if
end function
%>