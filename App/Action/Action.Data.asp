﻿<%
class ActionData
	private sub Class_Initialize()
	
	end sub
	public sub list
		Call index()
	end sub
	public function show(var)
		show = Mo.fetch("Home:Test")
	end function
	public sub index
		'读出所有数据，并按照每页5条记录分页，最后将数据赋值到List变量
		'只选择id和name字段：Model__("Public","id").select("id,name").limit(F.Get.Int("page",1),5).query().assign "List"
		Model__("Public").cache().limit(F.Get.Int("page",1),5).orderby("id desc").query().assign "List"

		'读取要编辑的记录ID
		Dim id:id = F.Get.Int("id",0)
		If id>0 Then
			'如果id大于0，查询数据，并将首条记录作为对象（true参数）赋值到Item变量，如果assign的第二个参数省略，则将字段和值分别赋值为全局变量
			Model__("Public").where("id=" & id).query().assign "Item",True
		Else
			'如果id不大于0，构造一条记录，用来新增，Record__的assign方法无其他构造
			Dim item:Set item = Record__()
			item.Set "name",""
			item.Set "age","0"
			item.Set "birthday",F.formatdate(Now(),"yyyy-MM-dd")
			item.Set "memo",""
			item.Set "grade","0"
			item.assign "Item"
		End If
		'将id赋值为全局变量
		Mo.Assign "id",id
		'将数据显示到模板，因为这里用的是Action.Data，所以需要用Home:来引用Actin.Home的模板
		Mo.display "Home:Data"
	end sub
	public sub modify
		Dim FV:Set FV = Mo("FormValidatee")
		FV.AddRule "id","required;numeric","ID错误"
		FV.AddRule "name","required;min-length:5;max-length:20"
		FV.AddRule "age","required;between:1,200"
		FV.AddRule "birthday","required;exp:/^(\d{4})\-(\d{1,2})\-(\d{1,2})$/"
		FV.AddRule "memo","max-length:50;"
		FV.AddRule "grade","required;numeric","级别必须为数字"
		If FV.Validate() Then
			If F.Post.Int("id",0)=0 Then
				'直接从post读数据，插入数据库，如果担心安全问题，请使用Record__构造一个字典，作为insert的参数传递
				Model__("Public","id").insert()
			else
				'直接从post读数据，更新数据库，如果担心安全问题，请使用Record__构造一个字典，作为update的参数传递
				Model__("Public","id").update()
			End if
			F.goto "?m=data","保存成功"
		Else
			F.goto "?m=data",FV.exception
		End If
	end sub
	public sub delete
		'删除记录
		Model__("Public","id").where("id=" & F.Get.Int("id")).delete()
		F.goto "?m=data","删除完成"
	end sub
end class
%>