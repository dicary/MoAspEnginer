﻿<%
class ActionForm
	private sub Class_Initialize()
	
	end sub
	public sub index
		Set FV = Mo("FormValidatee")
		
		'AddRule(表单名字,验证规则[,自定义的错误提示信息])
		FV.AddRule "name","required"
		FV.AddRule "title,password","required;min-length:6;max-length:10"
		FV.AddRule "repassword","equal:password","重复输入的密码错误"
		FV.AddRule "age","required;numeric;between:10,99;default:12"
		FV.AddRule "grade","required:false;exp:/^([\d\;]+)$/igm","请填写正确的级别"
		
		'支持几个快捷验证：qq/email/mobile/telphone/zipcode
		FV.AddRule "qq","qq"

		'输出格式化的验证规则，仅作调试用
		Mo.Assign "rules",FV.Rules()

		'这里添加临时数据用于验证，Validate会自动读取post的内容进行验证
		F.post "name","aien"
		F.post "title","aien22"
		F.post "grade","23"
		F.post "qq","34343"
		F.post "password","aien22"
		F.post "repassword","aien22"
		
		'执行验证
		If FV.Validate() Then
			Mo.Assign "result","Validate Succeed!"
		Else
			Mo.Assign "result","Validate Falied!<br />" + FV.exception
		End If
		Mo.display "Home:Form"
	end sub
end class
%>