MoAspEnginer V1.14
本框架部分功能参考ThinkPHP。

初衷：
	在ASP大势已去的时代，不想再在ASP上花太多精力，想写一个快速的ASP开发MVC框架，提高工作效率。


优势：
	1、单文件入口，M/A模式
	2、核心大部分JScript开发，
	3、将POST、GET进行可读写化操作，所有数据通过F.post或F.get进行读写
	4、支持CURL操作，扩展assign到Model和Record
	5、支持模板的跨模块、跨模板调用
	6、编译缓存、类库缓存，提高代码效率
	7、支持严格编译和非严格编译，严格编译需要变量的完全声明（即模板中使用的变量，都需要assign，特别是对象）
	8、支持类的静态调用和动态调用（静态调用只初始化一次，动态调用每次都初始化一个新类）
	9、支持update、insert的自动post调用（入库前可通过FormValidatee验证数据的有效性），例如：Model__("Public","id").insert()、Model__("Public","id").update()
	10、支持多数据库操作，只要编写多个数据库配置文件即可在程序中随意调用
	11、支持数据集的缓存，以json方式缓存到文件系统

缺陷：
	1、目前文档相对不完善
	2、调试不方便，需要一定的ASP基础
	3、核心大部分为JScript，不熟悉JScript的人员去修改核心有一定困难

QQ群：127430216-MoBlog交流(MoAspEnginer) 
帮助文档：http://www.9fn.net/help
最新版本：http://www.9fn.net/post/52.html

2013-11-08(v1.14)
增加：新增静态类库MoLibJsonParser，借鉴JSON官方的Json2文件，支持对json代码的解析和解析。

2013-11-06(v1.13)
增加：核心运行库增加A方法，用来加载特定的Action模块，例如：Mo.A("Data").show()，就是调用Action.Data里面的show方法
增加：核心运行库增加Status属性，用来设置HTTP状态，调用Mo.display方法时，本属性有效

2013-11-05(v1.12)
增加：函数占位符“...”，支持参数位置的定义，例如{$title:func=2,...}，{$title:func=2,...,3}，“...”的位置会被title变量替换

2013-11-01(v1.11)
修正：数据缓存时日期读写的bug

2013-10-31(v1.1)
1、增加Model数据文件缓存功能，通过cache()方法开启，例如Model__("Public","id").cache().query()
2、配置文件增加MO_MODEL_CACHE参数全局控制是否开启Model数据文件缓存
3、增加App的cache目录，用来保存编译缓存和Model数据缓存
4、Mo增加ModelCacheExists、ModelCacheSave、ModelCacheLoad、ModelCacheDelete、ModelCacheClear方法，用来管理Model数据缓存

2013-10-23(v1.06):
1、增加Form表单验证类；
2、F.string增加trim,trimLeft,trimRight,startWith,endWith方法
3、F.string增加format方法，重载F.format
